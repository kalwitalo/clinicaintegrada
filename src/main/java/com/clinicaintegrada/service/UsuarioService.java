package com.clinicaintegrada.service;


import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.clinicaintegrada.dao.UsuarioDAO;
import com.clinicaintegrada.model.Usuario;

@RequestScoped
public class UsuarioService {

    @Inject
    private UsuarioDAO usuarioDAO;

    public List<Usuario> listar(){
        return usuarioDAO.listar();
    }
}
