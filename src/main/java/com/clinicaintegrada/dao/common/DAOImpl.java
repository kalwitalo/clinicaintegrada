package com.clinicaintegrada.dao.common;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

public class DAOImpl<T, ID extends Serializable> implements IDAO<T, ID> {

    @Inject
    private EntityManager entityManager;

    @Override
    public void save(T entity) {
        entityManager.persist(entity);
    }

    @Override
    public T update(T entity) {
        return entityManager.merge(entity);
    }

    @Override
    public void remove(Serializable id, Class<T> clazz) {
        T obj = findById(id, clazz);
        entityManager.remove(obj);
    }

    @Override
    public T findById(Serializable id, Class<T> clazz) {
        return entityManager.find(clazz, id);
    }

    @Override
    public List<T> select(StringBuffer query) {
        return entityManager.createQuery(query.toString()).getResultList();
    }

    @Override
    public List<T> select(StringBuffer query, Hashtable<String, Object> parameters) {
        Enumeration keys = parameters.keys();
        Query consulta = entityManager.createQuery(query.toString());

        aplicarParametros(parameters, keys, consulta);

        return consulta.getResultList();
    }

    @Override
    public List<T> select(StringBuffer query, Hashtable<String, Object> parameters, Integer firsResult, Integer maxResults) {
        Enumeration keys = parameters.keys();
        Query consulta = entityManager.createQuery(query.toString());

        aplicarPaginacao(firsResult, maxResults, consulta);
        aplicarParametros(parameters, keys, consulta);

        return consulta.getResultList();
    }

    @Override
    public List<T> select(StringBuffer query, Integer firsResult, Integer maxResults) {
        Query consulta = entityManager.createQuery(query.toString());

        aplicarPaginacao(firsResult, maxResults, consulta);

        return consulta.getResultList();
    }
    
    public EntityManager getEntityManager(){
    	return this.entityManager;
    }

    private void aplicarParametros(Hashtable<String, Object> parameters, Enumeration keys, Query consulta) {
        while (keys.hasMoreElements()) {
            String key = keys.nextElement().toString();

            consulta.setParameter(key, parameters.get(key));
        }
    }

    private void aplicarPaginacao(Integer firsResult, Integer maxResults, Query consulta) {
        consulta.setMaxResults(maxResults);
        consulta.setFirstResult(firsResult);
    }
}
