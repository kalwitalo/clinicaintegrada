package com.clinicaintegrada.dao;

import java.util.List;

import javax.enterprise.context.RequestScoped;

import com.clinicaintegrada.dao.common.DAOImpl;
import com.clinicaintegrada.model.Usuario;

@RequestScoped
public class UsuarioDAO extends DAOImpl<Usuario, Integer> {

    public List<Usuario> listar(){
        StringBuffer stringBuffer = new StringBuffer("select u from Usuario u");
        return super.select(stringBuffer);
    }

}
