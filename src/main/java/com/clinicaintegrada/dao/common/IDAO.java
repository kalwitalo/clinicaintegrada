package com.clinicaintegrada.dao.common;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.List;

public interface IDAO<T, ID extends Serializable> {

    void save(T entity);

    T update(T entity);

    void remove(Serializable id, Class<T> clazz);

    T findById(Serializable id, Class<T> clazz);

    List<T> select(StringBuffer sql);

    List<T> select(StringBuffer query, Hashtable<String, Object> parameters);

    List<T> select(StringBuffer query, Integer firsResult, Integer maxResults);

    List<T> select(StringBuffer query, Hashtable<String, Object> parameters, Integer firsResult, Integer maxResults);
}
