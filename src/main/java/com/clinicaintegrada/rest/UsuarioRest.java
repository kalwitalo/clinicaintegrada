package com.clinicaintegrada.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.clinicaintegrada.model.Usuario;
import com.clinicaintegrada.service.UsuarioService;

@Path("/usuarios")
@Produces("application/json")
@Consumes("application/json")
public class UsuarioRest {

    @Inject
    private UsuarioService usuarioSrv;

    @GET
    @Path("/")
    public List<Usuario> get() {
        return usuarioSrv.listar();
    }
}
